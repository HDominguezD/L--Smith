// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DynamicTypeCharacter.generated.h"

UENUM(BlueprintType)
enum class ECharacterType : uint8
{
	VE_Red UMETA(DisplayName = "Red"),
	VE_Green UMETA(DisplayName = "Green"),
	VE_Blue UMETA(DisplayName = "Blue"),
	VE_Null UMETA(DisplayName = "Null")
};

UCLASS()
class ELEMENTALBLACKSMITH_API ADynamicTypeCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADynamicTypeCharacter();

	UFUNCTION(BlueprintCallable, Category="ADynamicTypeCharacter")
	void SetCharacterType(ECharacterType CharacterType);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
		ECharacterType m_CharacterType;
	UPROPERTY(VisibleAnywhere, Category = SkeletalMesh)
		class USkeletalMeshComponent* PlayerMesh;
	UPROPERTY(VisibleAnywhere, Category = UMaterial)
		class UMaterial* PlayerMaterial;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
