// Fill out your copyright notice in the Description page of Project Settings.


#include "DynamicTypeCharacter.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"

// Sets default values
ADynamicTypeCharacter::ADynamicTypeCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_CharacterType = ECharacterType::VE_Null;
}

void ADynamicTypeCharacter::SetCharacterType(ECharacterType CharacterType)
{
	m_CharacterType = CharacterType;
	//PlayerMesh = GetMesh();

		if (m_CharacterType == ECharacterType::VE_Red)
		{
			//PlayerMaterial = LoadObject<UMaterial>(NULL, TEXT("/Game/LSmith/Materials/Red.Red"), NULL, LOAD_None, NULL);
		}
		else if (m_CharacterType == ECharacterType::VE_Green)
		{
			//PlayerMaterial = LoadObject<UMaterial>(NULL, TEXT("/Game/LSmith/Materials/Green.Green"), NULL, LOAD_None, NULL);
		}
		else if (m_CharacterType == ECharacterType::VE_Blue)
		{
			//PlayerMaterial = LoadObject<UMaterial>(NULL, TEXT("/Game/LSmith/Materials/Blue.Blue"), NULL, LOAD_None, NULL);
		}
		else if (m_CharacterType == ECharacterType::VE_Null)
		{
			//PlayerMaterial = LoadObject<UMaterial>(NULL, TEXT("/Game/LSmith/Materials/Gray.Gray"), NULL, LOAD_None, NULL);
		}

		//PlayerMesh->SetMaterial(0, PlayerMaterial);
}

// Called when the game starts or when spawned
void ADynamicTypeCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerMesh = GetMesh();
	SetCharacterType(m_CharacterType);
}

// Called every frame
void ADynamicTypeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADynamicTypeCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

